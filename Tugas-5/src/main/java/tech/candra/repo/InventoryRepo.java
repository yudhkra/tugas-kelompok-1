package tech.candra.repo;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import tech.candra.models.Inventory;

@ApplicationScoped
public class InventoryRepo {
    
    Map<Integer, Inventory> inventoriesMap = new HashMap<>();

    private Integer counterId = 0;

    private boolean diRiset = false;

    private void generateId() {
        if (reset()) {
            counterId = 1;
        } else {
        counterId++;
        }
    }

    public Map<Integer, Inventory> getInventories() {
        return inventoriesMap;
    }

    public Inventory getInventory(Integer id) {
        return inventoriesMap.get(id);
    }

    public void addInventory(Inventory theInventory) {
        generateId();
        inventoriesMap.put(counterId, theInventory);
    }

    public void updateInventory(Integer id, Inventory inventory) {
        inventoriesMap.replace(id, inventory);
    }

    public void deleteInventory(Integer id) {
        inventoriesMap.remove(id);
    }

    public void deleteInventories() {
        diRiset = true;
        inventoriesMap.clear();
    }

    private boolean reset() {
        if (inventoriesMap.size() == 1) {
            diRiset = false;
        }
        return diRiset;
    }


}

