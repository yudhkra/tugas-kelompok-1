package tech.candra;

import tech.candra.models.Mugiwara;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Path("mugiwara")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class MugiwaraResource {

    public Map<Integer, Mugiwara> kumpulanMugiwara = new HashMap<>();
    public Integer id = 1;

    @Inject
    Validator validator;

    //Menampung Kumpulan Object
    public MugiwaraResource() {
        Mugiwara nakama1 = new Mugiwara("Monkey D Luffi", "Akuma No Mi", "Housoku No Haki - Kenbunsouku Haki - Bousoku Haki", 100,20,100D);
        Mugiwara nakama2 = new Mugiwara("Roronoa Zoro", "Santoryu", "Housoku No Haki - Bousoku Haki", 95, 25, 100D);
        Mugiwara nakama3 = new Mugiwara("Vinsmoke Sanji", "Double Jambe", "Kenbunsouku Haki - Bousoku Haki", 90, 21, 98D);

        kumpulanMugiwara.put(id++, nakama1);
        kumpulanMugiwara.put(id++, nakama2);
        kumpulanMugiwara.put(id++, nakama3);
    }

    @GET //Menampilkan daftar objek secara keseluruhan
    @Operation(summary = "Get All Mugiwara", description = "Access to all mugiwara data")
    public Map<Integer, Mugiwara> getKumpulanMugiwara() {
        return kumpulanMugiwara;
    }

    @GET // Menampilkan Objek dengan id tertentu
    @Path("{id}")
    @Operation(summary = "Access Mugiwara", description = "Access a specific mugiwara using an ID number")
    public Mugiwara getMugiwaraById(@PathParam("id") Integer id) {
        System.out.println(kumpulanMugiwara.get(id));
        return kumpulanMugiwara.get(id);
    }

    @POST //Menambahkan data objek baru
    @Path("add")
    @Operation(summary = "Add an Mugiwara", description = "Create/add an mugiwara at a time and ID number will generated automatically")
    public Object addMugiwara (@Valid Mugiwara mugiwara) {
        Set<ConstraintViolation<Mugiwara>> violations = validator.validate(mugiwara);
        if (violations.isEmpty()) {
            kumpulanMugiwara.put(id++, mugiwara);
            return ("Berhasil Ditambahkan");
        } else {
            return (violations);
        }
    }

    @PUT //Update data objek baru dengan id tertentu
    @Path("{id}")
    @Operation(summary = "Update an Mugiwara", description = "Update an mugiwara at a time using an ID number")
    public Object updateMugiwara( @PathParam("id") Integer id, @Valid Mugiwara newMugiwara){
        Map<String, Mugiwara> result =  new HashMap<>();
        result.put("oldMugiwara", kumpulanMugiwara.get(id));
        kumpulanMugiwara.replace(id, newMugiwara);
        result.put("newMugiwara", kumpulanMugiwara.get(id));
        return result;
    }

    @DELETE //Delete data objek dengan id tertentu
    @Path("{id}")
    @Operation(summary = "Delete an Mugiwara", description = "Delete an mugiwara at a time using an ID number")
    public Mugiwara deleteMugiwara(@PathParam("id") Integer id) {
        Mugiwara mugiwara = kumpulanMugiwara.get(id);
        kumpulanMugiwara.remove(id);
        return mugiwara;
    }

    @DELETE // Delete keseluruhan data dengan mengembalikan id menjadi 1
    @Path("del")
    @Operation(summary = "Delete all Mugiwara", description = "Delete all mugiwara data and it will reset the ID")
    public String deleteAllMugiwara() {
        kumpulanMugiwara.clear();
        id = 1; 
        return "Delete Berhasil";
    }  
}