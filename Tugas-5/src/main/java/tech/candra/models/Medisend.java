package tech.candra.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

//struktur data yang ditampung
public class Medisend {

    @NotBlank (message = "Nama harus ada!")
    public String name;

    @NotBlank (message = "Warna harus ada!")
    public String colour;

    @NotNull(message = "Tidak boleh kosong!")
    @Min(value = 1, message = "Minimal 1")
    public Integer jumlah;
    
    @Min(value = 0, message = "Nikotin Terbawah 0")
    @Max(value = 1000000, message = "Harga maksimal 1000000")
    public Integer harga;

    public Medisend() {
    }

    public Medisend(String name, String colour, Integer jumlah, Integer harga) {
        this.name = name;
        this.colour = colour;
        this.jumlah = jumlah;
        this.harga = harga;
    }

    }