package tech.candra.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class JenisRoko {

    @NotBlank (message = "Nama Roko Harus Ada!, ")
    public String nama;
    
    @NotNull(message = "Tidak boleh kosong!")
    @Min(value = 0, message = "Jumlah Terbawah 0")
    @Max(value = 100, message = "Jumlah Teringgi 100, ")
    public Integer jumlah;

    @NotNull(message = "Tidak boleh kosong!")
    @Min(value = 1000, message = "Harga Terbawah 1000")
    public Integer harga;

    @NotNull(message = "Tidak boleh kosong!")
    @Min(value = 0, message = "Nikotin Terbawah 0")
    @Max(value = 3, message = "Nikotin Teringgi 3, ")
    public Integer nikotin;


    public JenisRoko() {

    }


    public JenisRoko(String nama, Integer jumlah, Integer harga, Integer nikotin) {
        this.nama = nama;
        this.jumlah = jumlah;
        this.harga = harga;
        this.nikotin = nikotin;
    }

}