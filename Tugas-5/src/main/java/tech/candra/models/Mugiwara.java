
package tech.candra.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


public class Mugiwara {
     
    //Validasi Judul
    //Memberikan Pesan error
    @NotBlank (message = "Nama Nakama Harus Ada!, ")
    public String nama;

    @NotBlank (message = "Weapon Nakama Harus Ada!, ")
    public String weapon;

    @NotBlank (message = "Armor Nakama Harus Ada!, ")
    public String armor;

    @Min(value = 0, message = "Health Nakama Terkecil 0 ")
    @Max(value = 100, message = "Health Nakama Terbesar 100, ")
    public Integer health;

    @Min(value = 0, message = "Age Nakama Terbawah 0")
    @Max(value = 80, message = "Age Nakama Teringgi 80, ")
    public Integer age;
    
    @Min(value = 0, message = "Attack Nakama Terkecil 0")
    @Max(value = 100, message = "Attack Nakama Terbesar 100, ")
    public Double attack;

    public Mugiwara() {}

    public Mugiwara(String nama, String weapon, String armor, Integer health, Integer age, Double attack) {
        this.nama = nama;
        this.weapon = weapon;
        this.armor = armor;
        this.health = health;
        this.age = age;
        this.attack =  attack;
    }
}
