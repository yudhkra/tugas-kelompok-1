package tech.candra;

import tech.candra.models.Medisend;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Path("medisend")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class MedisendResource {

    public Map<Integer, Medisend> kumpulanMedisend = new HashMap<>();
    public Integer id = 1;

    @Inject
    Validator validator;

    //Menampung Kumpulan Object
    public MedisendResource() {
        Medisend roko1 = new Medisend("Barang satu", "kuning", 250000, 1);
        Medisend roko2 = new Medisend("Barang dua", "merah", 150000, 3);
        Medisend roko3 = new Medisend("Barang tiga", "hijau", 200000, 2);
        

        kumpulanMedisend.put(id++, roko1);
        kumpulanMedisend.put(id++, roko2);
        kumpulanMedisend.put(id++, roko3);
    }

    @GET
    //Menampilkan daftar objek secara keseluruhan
    public Map<Integer, Medisend> getSemuaMedisend() {
        return kumpulanMedisend;
    }

    @GET
    @Path("{id}")
    // Menampilkan Objek dengan id tertentu
    public Medisend getMedisendById(@PathParam("id") Integer id) {
        System.out.println(kumpulanMedisend.get(id));
        return kumpulanMedisend.get(id);
    }

    @POST
    //Menambahkan data objek baru
    public Object addMedisend (@Valid Medisend medisend) {
        Set<ConstraintViolation<Medisend>> violations = validator.validate(medisend);
        if (violations.isEmpty()) {
            kumpulanMedisend.put(id++, medisend);
            return ("Berhasil Ditambahkan");
        } else {
            return (violations);
        }
    }

    @PUT
    @Path("{id}")
    //Update data objek baru dengan id tertentu
    public Object updateMedisend( @PathParam("id") Integer id, @Valid Medisend medisend){
        Map<String, Medisend> result =  new HashMap<>();
        result.put("roko", kumpulanMedisend.get(id));
        kumpulanMedisend.replace(id, medisend);
        result.put("roko", kumpulanMedisend.get(id));
        return result;
    }

    @DELETE
    @Path("{id}")
    //Delete data objek dengan id tertentu
    public Medisend deleteMedisend(@PathParam("id") Integer id) {
        Medisend rokoRoko = kumpulanMedisend.get(id);
        kumpulanMedisend.remove(id);
        return rokoRoko;
    }

    @DELETE
    // Delete keseluruhan data dengan mengembalikan id menjadi 1
    public String deleteAllJenis() {
        kumpulanMedisend.clear();
        id = 1; 
        return "Delete Berhasil";
    }

}