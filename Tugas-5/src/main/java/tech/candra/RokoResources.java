package tech.candra;

import tech.candra.models.JenisRoko;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Path("roko")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class RokoResources {

    public Map<Integer, JenisRoko> jenisRoko = new HashMap<>();
    public Integer id = 1;

    @Inject
    Validator validator;

    //Menampung Kumpulan Object
    public RokoResources() {
        JenisRoko roko1 = new JenisRoko("Sampoerna", 10, 250000, 1);
        JenisRoko roko2 = new JenisRoko("Djarum", 10, 150000, 3);
        JenisRoko roko3 = new JenisRoko("Garpit", 10, 200000, 2);
        

        jenisRoko.put(id++, roko1);
        jenisRoko.put(id++, roko2);
        jenisRoko.put(id++, roko3);
    }

    @GET
    //Menampilkan daftar objek secara keseluruhan
    public Map<Integer, JenisRoko> getSemuaJenisRoko() {
        return jenisRoko;
    }

    @GET
    @Path("{id}")
    // Menampilkan Objek dengan id tertentu
    public JenisRoko getJenisRokoById(@PathParam("id") Integer id) {
        System.out.println(jenisRoko.get(id));
        return jenisRoko.get(id);
    }

    @POST
    //Menambahkan data objek baru
    public Object addJenisRoko (@Valid JenisRoko roko) {
        Set<ConstraintViolation<JenisRoko>> violations = validator.validate(roko);
        if (violations.isEmpty()) {
            jenisRoko.put(id++, roko);
            return ("Berhasil Ditambahkan");
        } else {
            return (violations);
        }
    }

    @PUT
    @Path("{id}")
    //Update data objek baru dengan id tertentu
    public Object updateJenisRoko( @PathParam("id") Integer id, @Valid JenisRoko roko){
        Map<String, JenisRoko> result =  new HashMap<>();
        result.put("roko", jenisRoko.get(id));
        jenisRoko.replace(id, roko);
        result.put("roko", jenisRoko.get(id));
        return result;
    }

    @DELETE
    @Path("{id}")
    //Delete data objek dengan id tertentu
    public JenisRoko deleteJenisRoko(@PathParam("id") Integer id) {
        JenisRoko rokoRoko = jenisRoko.get(id);
        jenisRoko.remove(id);
        return rokoRoko;
    }

    @DELETE
    // Delete keseluruhan data dengan mengembalikan id menjadi 1
    public String deleteAllJenisRoko() {
        jenisRoko.clear();
        id = 1; 
        return "Delete Berhasil";
    }

}