package tech.candra;

import java.util.Map;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.Operation;

import tech.candra.models.Inventory;
import tech.candra.repo.InventoryRepo;

@Path("/inventory")
public class InventoryResources {

    @Inject
    InventoryRepo inventoryRepo;

    @GET
    @Operation(
        summary = "Get inventories", 
        description = "Access to all inventory data")
    @Produces(MediaType.APPLICATION_JSON)
    public Map<Integer, Inventory> getInventories() {
        return inventoryRepo.getInventories();
    }

    @GET
    @Path("/{id}")
    @Operation(
        summary = "Access an inventory", 
        description = "Access a specific inventory using an ID number")
    @Produces(MediaType.APPLICATION_JSON)
    public Inventory getInventory(@PathParam("id") Integer id) {
        return inventoryRepo.getInventory(id);
    }

    @POST
    @Path("/add")
    @Operation(
        summary = "Add an inventory", 
        description = "Create/add an inventory at a time and ID number will generated automatically")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addInventory(@Valid Inventory inventory) {
        inventoryRepo.addInventory(inventory);
    }
    
    @PUT
    @Path("/{id}/set")
    @Operation(
        summary = "Update an inventory", 
        description = "Update an inventory at a time using an ID number")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateInventory(@Valid @PathParam("id") Integer id, Inventory inventory) {
        inventoryRepo.updateInventory(id, inventory);
    }

    @DELETE
    @Path("/{id}/delete")
    @Operation(
        summary = "Delete an inventory", 
        description = "Delete an inventory at a time using an ID number")
    @Consumes(MediaType.TEXT_PLAIN)
    public void deleteInventory(@PathParam("id") Integer id) {
        inventoryRepo.deleteInventory(id);
    }

    @DELETE
    @Path("/delete")
    @Operation(
        summary = "Delete all inventories", 
        description = "Delete all inventory data and it will reset the ID")
    public void deleteAll() {
        inventoryRepo.deleteInventories();
    }

}