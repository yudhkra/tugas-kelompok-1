package tech.candra.models;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

@Entity
@Table(name = "medisend")
public class Medisend {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Schema(readOnly = true)
    public Long id;
    
    @NotBlank (message = "Nama Medisend Harus Ada!, ")
    public String name;
    
    @NotBlank (message = "farmasi medisend harus ada!, ")
    public String farmasi;
    
    @NotBlank (message = "vitacimin medisend Harus Ada!, ")
    public String obat;
        
    @NotNull(message = "health harus ada!, ")
    @Min(message = "Health 100", value = 100)
    public Integer health;
    
    @NotNull(message = "Age medisend  harus diisi!, ")
    @Min(value = 0, message = "Age medisend Terkecil 0")
    public Integer age;
        
    @NotNull(message = "Attack Medisend harus disi!, ")
    @Min(value = 0, message = "Attack Medisend Terkecil 0")
    public Double attack;


}
