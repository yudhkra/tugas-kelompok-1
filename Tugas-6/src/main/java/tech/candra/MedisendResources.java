package tech.candra;

import java.util.ArrayList;
import java.util.List;
 
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import tech.candra.models.Medisend;

import javax.ws.rs.Consumes;

@Path("/medisend")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MedisendResources {

    @Inject
   EntityManager em;
 
   @GET
   public List<Medisend> geMedisends() {
       List<Medisend> kumpulanMedisend = new ArrayList<>();
       kumpulanMedisend = em.createQuery("SELECT i FROM Medisend i", Medisend.class).getResultList();
       return kumpulanMedisend;
   }
 
   @POST
   @Path("/insert")
   @Transactional
   public List<Medisend> insertMedisend(@Valid Medisend medisend) {
       List<Medisend> kumpulanMedisend = new ArrayList<>();
       em.persist(medisend);
       kumpulanMedisend =
       em.createQuery("SELECT i FROM Medisend i", Medisend.class)
       .getResultList();
       return kumpulanMedisend;
   }
 
    @GET
  @Path("{id}")
  public Medisend getMedisend(@PathParam("id") Long id) {
      Medisend medisend =
      em.createQuery("SELECT i FROM Medisend i WHERE i.id = :id", Medisend.class)
      .setParameter("id", id).getSingleResult();
      return medisend;
  }
 
 
   @DELETE
   @Path("{id}/delete")
   @Transactional
   public List<Medisend> deletMedisend(@PathParam("id") Long id) {
       List<Medisend> kumpulanMedisend = new ArrayList<>();
       Medisend medisend = em.createQuery("SELECT i FROM Medisend i WHERE i.id = :id", Medisend.class).setParameter("id", id).getSingleResult();
       em.remove(medisend);
       kumpulanMedisend =
       em.createQuery("SELECT i FROM Medisend i", Medisend.class)
       .getResultList();
       return kumpulanMedisend;
   }
 
   @PUT
   @Path("/update/{id}")
   @Transactional
   public Medisend updateMedisends(@PathParam("id") Long id, @Valid Medisend medisend) {
       Medisend medisendOld =
       em.createQuery("SELECT i FROM Medisend i WHERE i.id = :id", Medisend.class)
       .setParameter("id", id).getSingleResult();
 
       medisendOld.age = medisend.age;
       medisendOld.attack = medisend.attack;
       medisendOld.farmasi = medisend.farmasi;
       medisendOld.health = medisend.health;
       medisendOld.name = medisend.name;
       medisend.obat = medisend.obat;
 
       return medisendOld;
   }
 
   @DELETE
   @Path("/deletes")
   @Transactional
   public String deleteMedisend() {
    em.createQuery("DELETE FROM Medisend").executeUpdate();
    return "Semua data Medisend terhapus";
   }

    
}
